import com.company.dao.TableDao;
import com.company.model.TableEntity;
import org.hibernate.hql.spi.id.TableBasedDeleteHandlerImpl;

import java.sql.SQLException;
import java.util.List;

public class Main {

    public static void main(final String[] args) throws SQLException {
        TableDao tableDao=new TableDao();
        List<TableEntity> list=tableDao.getAllTables();
        for (TableEntity tableEntity:list) {
            System.out.println(tableEntity.getTableid());
        }


    }
}
