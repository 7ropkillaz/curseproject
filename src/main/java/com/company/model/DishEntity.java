package com.company.model;

import javax.persistence.*;

@Entity
@Table(name = "dish", schema = "public", catalog = "curserest")
public class DishEntity {
    private int dishid;
    private String dishname;
    private String ingredients;
    private int weight;
    private int price;

    @Id
    @Column(name = "dishid", nullable = false)
    public int getDishid() {
        return dishid;
    }

    public void setDishid(int dishid) {
        this.dishid = dishid;
    }

    @Basic
    @Column(name = "dishname", nullable = false, length = -1)
    public String getDishname() {
        return dishname;
    }

    public void setDishname(String dishname) {
        this.dishname = dishname;
    }

    @Basic
    @Column(name = "ingredients", nullable = false, length = -1)
    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    @Basic
    @Column(name = "weight", nullable = false)
    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Basic
    @Column(name = "price", nullable = false)
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DishEntity that = (DishEntity) o;

        if (dishid != that.dishid) return false;
        if (weight != that.weight) return false;
        if (price != that.price) return false;
        if (dishname != null ? !dishname.equals(that.dishname) : that.dishname != null) return false;
        if (ingredients != null ? !ingredients.equals(that.ingredients) : that.ingredients != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dishid;
        result = 31 * result + (dishname != null ? dishname.hashCode() : 0);
        result = 31 * result + (ingredients != null ? ingredients.hashCode() : 0);
        result = 31 * result + weight;
        result = 31 * result + price;
        return result;
    }
}
