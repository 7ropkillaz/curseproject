package com.company.model;

import javax.persistence.*;

@Entity
@Table(name = "waiter", schema = "public", catalog = "curserest")
public class WaiterEntity {
    private int waiterid;
    private String fio;

    @Id
    @Column(name = "waiterid", nullable = false)
    public int getWaiterid() {
        return waiterid;
    }

    public void setWaiterid(int waiterid) {
        this.waiterid = waiterid;
    }

    @Basic
    @Column(name = "fio", nullable = false, length = -1)
    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WaiterEntity that = (WaiterEntity) o;

        if (waiterid != that.waiterid) return false;
        if (fio != null ? !fio.equals(that.fio) : that.fio != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = waiterid;
        result = 31 * result + (fio != null ? fio.hashCode() : 0);
        return result;
    }
}
