package com.company.model;

import javax.persistence.*;

@Entity
@Table(name = "tablelist", schema = "public", catalog = "curserest")
@IdClass(TablelistEntityPK.class)
public class TablelistEntity {
    private int waiterid;
    private int tableid;

    @Id
    @Column(name = "waiterid", nullable = false)
    public int getWaiterid() {
        return waiterid;
    }

    public void setWaiterid(int waiterid) {
        this.waiterid = waiterid;
    }

    @Id
    @Column(name = "tableid", nullable = false)
    public int getTableid() {
        return tableid;
    }

    public void setTableid(int tableid) {
        this.tableid = tableid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TablelistEntity that = (TablelistEntity) o;

        if (waiterid != that.waiterid) return false;
        if (tableid != that.tableid) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = waiterid;
        result = 31 * result + tableid;
        return result;
    }
}
