package com.company.model;

import javax.persistence.*;

@Entity
@Table(name = "table", schema = "public", catalog = "curserest")
public class TableEntity {
    private int tableid;
    private int tablenum;

    @Basic
    @Id
    @Column(name = "tableid", nullable = false)
    public int getTableid() {
        return tableid;
    }

    public void setTableid(int tableid) {
        this.tableid = tableid;
    }

    @Basic
    @Column(name = "tablenum", nullable = false)
    public int getTablenum() {
        return tablenum;
    }

    public void setTablenum(int tablenum) {
        this.tablenum = tablenum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TableEntity that = (TableEntity) o;

        if (tableid != that.tableid) return false;
        if (tablenum != that.tablenum) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tableid;
        result = 31 * result + tablenum;
        return result;
    }
}
