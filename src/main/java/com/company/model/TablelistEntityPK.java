package com.company.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class TablelistEntityPK implements Serializable {
    private int waiterid;
    private int tableid;

    @Column(name = "waiterid", nullable = false)
    @Id
    public int getWaiterid() {
        return waiterid;
    }

    public void setWaiterid(int waiterid) {
        this.waiterid = waiterid;
    }

    @Column(name = "tableid", nullable = false)
    @Id
    public int getTableid() {
        return tableid;
    }

    public void setTableid(int tableid) {
        this.tableid = tableid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TablelistEntityPK that = (TablelistEntityPK) o;

        if (waiterid != that.waiterid) return false;
        if (tableid != that.tableid) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = waiterid;
        result = 31 * result + tableid;
        return result;
    }
}
