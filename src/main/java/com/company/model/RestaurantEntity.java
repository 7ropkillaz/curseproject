package com.company.model;

import javax.persistence.*;

@Entity
@Table(name = "restaurant", schema = "public", catalog = "curserest")
public class RestaurantEntity {
    private int restaurantid;
    private String address;
    private String restaurantname;

    @Id
    @Column(name = "restaurantid", nullable = false)
    public int getRestaurantid() {
        return restaurantid;
    }

    public void setRestaurantid(int restaurantid) {
        this.restaurantid = restaurantid;
    }

    @Basic
    @Column(name = "address", nullable = false, length = -1)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "restaurantname", nullable = false, length = -1)
    public String getRestaurantname() {
        return restaurantname;
    }

    public void setRestaurantname(String restaurantname) {
        this.restaurantname = restaurantname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RestaurantEntity that = (RestaurantEntity) o;

        if (restaurantid != that.restaurantid) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (restaurantname != null ? !restaurantname.equals(that.restaurantname) : that.restaurantname != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = restaurantid;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (restaurantname != null ? restaurantname.hashCode() : 0);
        return result;
    }
}
