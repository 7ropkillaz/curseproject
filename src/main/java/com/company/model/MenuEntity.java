package com.company.model;

import javax.persistence.*;

@Entity
@Table(name = "menu", schema = "public", catalog = "curserest")
public class MenuEntity {
    private int menuid;
    private String dishtype;
    private String dishdescription;

    @Id
    @Column(name = "menuid", nullable = false)
    public int getMenuid() {
        return menuid;
    }

    public void setMenuid(int menuid) {
        this.menuid = menuid;
    }

    @Basic
    @Column(name = "dishtype", nullable = false, length = -1)
    public String getDishtype() {
        return dishtype;
    }

    public void setDishtype(String dishtype) {
        this.dishtype = dishtype;
    }

    @Basic
    @Column(name = "dishdescription", nullable = true, length = -1)
    public String getDishdescription() {
        return dishdescription;
    }

    public void setDishdescription(String dishdescription) {
        this.dishdescription = dishdescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MenuEntity that = (MenuEntity) o;

        if (menuid != that.menuid) return false;
        if (dishtype != null ? !dishtype.equals(that.dishtype) : that.dishtype != null) return false;
        if (dishdescription != null ? !dishdescription.equals(that.dishdescription) : that.dishdescription != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = menuid;
        result = 31 * result + (dishtype != null ? dishtype.hashCode() : 0);
        result = 31 * result + (dishdescription != null ? dishdescription.hashCode() : 0);
        return result;
    }
}
