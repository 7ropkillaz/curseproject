package com.company.dao;


import com.company.model.MenuEntity;
import com.company.util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class MenuDao {
    public MenuDao(){}

    public void addMenu(MenuEntity menuEntity){
        Session session= HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.save(menuEntity);
        transaction.commit();
        session.close();
    }
    public List<MenuEntity> getAllMenu(){
        List<MenuEntity> menuEntity =(List<MenuEntity>) HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From MenuEntity")
                .list();
        return menuEntity;
    }
    public void delete(MenuEntity menuEntity){
        Session session=HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.delete(menuEntity);
        transaction.commit();
        session.close();
    }
    public MenuEntity findMenuById(int id){
       MenuEntity menuEntity=HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(MenuEntity.class, id);
        return menuEntity;
    }
    public void updateMenu(MenuEntity menuEntity){
        Session session=HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.update(menuEntity);
        transaction.commit();
        session.close();
    }
}
