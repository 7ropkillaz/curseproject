package com.company.dao;

import com.company.model.DishEntity;
import com.company.util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class DishDao {
    public DishDao(){}

    public void addDish(DishEntity dishEntity){
        Session session= HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.save(dishEntity);
        transaction.commit();
        session.close();
    }
    public List<DishEntity> getAllDish(){
        List<DishEntity> dishEntity =(List<DishEntity>) HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From DishEntity")
                .list();
        return dishEntity;
    }
    public void delete(DishEntity dishEntity){
        Session session=HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.delete(dishEntity);
        transaction.commit();
        session.close();
    }
    public DishEntity findDishById(int id){
        DishEntity dishEntity=HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(DishEntity.class, id);
        return dishEntity;
    }
    public void updateDish(DishEntity dishEntity){
        Session session=HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.update(dishEntity);
        transaction.commit();
        session.close();
    }
}
