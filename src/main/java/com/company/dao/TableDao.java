package com.company.dao;

import com.company.model.TableEntity;
import com.company.util.HibernateSessionFactoryUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class TableDao {
    public TableDao(){}

    public void addTable(TableEntity tableEntity){
        Session session= HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.save(tableEntity);
        transaction.commit();
        session.close();
    }
    public List<TableEntity> getAllTables(){
        List<TableEntity> tableEntity =(List<TableEntity>) HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From TableEntity")
                .list();
        return tableEntity;
    }
    public void delete(TableEntity tableEntity){
        Session session=HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.delete(tableEntity);
        transaction.commit();
        session.close();
    }
    public TableEntity findTableById(int id){
        TableEntity tableEntity=HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(TableEntity.class, id);
        return tableEntity;
    }
    public void updateTable(TableEntity tableEntity){
        Session session=HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.update(tableEntity);
        transaction.commit();
        session.close();
    }


}
