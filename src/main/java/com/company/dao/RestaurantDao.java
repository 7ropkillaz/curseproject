package com.company.dao;


import com.company.model.RestaurantEntity;
import com.company.util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class RestaurantDao {
    public RestaurantDao(){}

    public void addRestaurant(RestaurantEntity restaurantEntity){
        Session session= HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.save(restaurantEntity);
        transaction.commit();
        session.close();
    }
    public List<RestaurantEntity> getAllRestaurants(){
        List<RestaurantEntity> restaurantEntity =(List<RestaurantEntity>) HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From RestaurantEntity")
                .list();
        return restaurantEntity;
    }
    public void delete(RestaurantEntity restaurantEntity){
        Session session=HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.delete(restaurantEntity);
        transaction.commit();
        session.close();
    }
    public RestaurantEntity findRestaurantById(int id){
        RestaurantEntity restaurantEntity=HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(RestaurantEntity.class, id);
        return restaurantEntity;
    }
    public void updateRestaurant(RestaurantEntity restaurantEntity){
        Session session=HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.update(restaurantEntity);
        transaction.commit();
        session.close();
    }
}
