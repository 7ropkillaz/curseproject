package com.company.dao;


import com.company.model.WaiterEntity;
import com.company.util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class WaiterDao {
    public WaiterDao(){}

    public void addWaiter(WaiterEntity waiterEntity){
        Session session= HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.save(waiterEntity);
        transaction.commit();
        session.close();
    }
    public List<WaiterEntity> getAllWaiters(){
        List<WaiterEntity> waiterEntity=(List<WaiterEntity>) HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From WaiterEntity")
                .list();
        return waiterEntity;
    }
    public void delete(WaiterEntity waiterEntity){
        Session session=HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.delete(waiterEntity);
        transaction.commit();
        session.close();
    }
    public WaiterEntity findWaiterById(int id){
        WaiterEntity waiterEntity=HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(WaiterEntity.class, id);
        return waiterEntity;
    }
    public void updateWaiter(WaiterEntity waiterEntity){
        Session session=HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.update(waiterEntity);
        transaction.commit();
        session.close();
    }
}
