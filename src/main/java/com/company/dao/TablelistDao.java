package com.company.dao;


import com.company.model.TablelistEntity;
import com.company.util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class TablelistDao {
    public TablelistDao(){}

    public void addTablelist(TablelistEntity tablelistEntity){
        Session session= HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.save(tablelistEntity);
        transaction.commit();
        session.close();
    }
    public List<TablelistEntity> getAllTableslist(){
        List<TablelistEntity> tablelistEntity =(List<TablelistEntity>) HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From TablelistEntity")
                .list();
        return tablelistEntity;
    }
    public void delete(TablelistEntity tablelistEntity){
        Session session=HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.delete(tablelistEntity);
        transaction.commit();
        session.close();
    }
    public TablelistEntity findTablelistById(int id){
        TablelistEntity tablelistEntity=HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(TablelistEntity.class, id);
        return tablelistEntity;
    }
    public void updatelistTable(TablelistEntity tablelistEntity){
        Session session=HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.update(tablelistEntity);
        transaction.commit();
        session.close();
    }


}
