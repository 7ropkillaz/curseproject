package com.company.util;

import com.company.model.DishEntity;
import com.company.model.MenuEntity;
import com.company.model.RestaurantEntity;
import com.company.model.TableEntity;
import com.company.model.TablelistEntityPK;
import com.company.model.WaiterEntity;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateSessionFactoryUtil {
    private static SessionFactory sessionFactory;

    private HibernateSessionFactoryUtil() {}

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(DishEntity.class);
                configuration.addAnnotatedClass(MenuEntity.class);
                configuration.addAnnotatedClass(RestaurantEntity.class);
                configuration.addAnnotatedClass(TableEntity.class);
                configuration.addAnnotatedClass(TablelistEntityPK.class);
                configuration.addAnnotatedClass(WaiterEntity.class);
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());

            } catch (Exception e) {
                System.out.println("Исключение!" + e);
            }
        }
        return sessionFactory;
    }
}
